Website
=======
http://icculus.org/smpeg/

License
=======
LGPL license (see the file source/COPYING)

Version
=======
svn revision 412

Source
======
svn://svn.icculus.org/smpeg/trunk

Requires
========
* SDL2